package com.javalessons.oop;

public class Main {
    public static void main(String[] args) {
        System.out.println("Dogs count " + Dog.getDogsCount());

        Dog lab = new Dog();
        lab.setName("Charley");
        lab.setBreed("LAbrador");
        lab.setSize(Size.AVARAGE);
        lab.bite();

        Dog shepard = new Dog();
        shepard.setName("Mike");
        shepard.setBreed("Shepard");
        shepard.setSize(Size.BIG);
        shepard.bite();

        Dog doberman = new Dog();
        doberman.setName("Jack");
        doberman.setBreed("Doberman");
        doberman.setSize(Size.BIG);
        doberman.bite();

        Size s = Size.SMALL;
        System.out.println(s);
        Size s1=Size.valueOf("BIG");
        System.out.println(s1);
        Size[] values = Size.values();
        for (int i=0; i<values.length; i++){
            System.out.println(values[i]);
        }
    }

}
