package com.javalessons.oop;

import static com.javalessons.oop.Size.*;

public class Dog {
    private static int dogsCount;
    public static final int PAWS = 4, TAIL = 1;
    private String name, breed;
    private Size size =Size.UNDEFINED;

    public Dog() {
        dogsCount++;
        System.out.println("Dogs is " + dogsCount);
    }


    public static int getDogsCount() {
        return dogsCount;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void bark() {
        switch (size){
            case BIG:
            case VERY_BIG:
                System.out.println("Wof - wof");
                break;
            case AVARAGE:
                System.out.println("RAf - Arf");
                break;
            case SMALL:
            case VERY_SMALL:
                System.out.println("RAf - Arf");
                break;
                default:
                    System.out.println("Dog's size udefined");
        }
    }

    public void bite() {
        if (dogsCount > 2) {
            System.out.println("Dogs is biting you");
        } else {
            bark();
        }
    }
}
